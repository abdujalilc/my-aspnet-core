﻿using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;

namespace CookieGetSet.Controllers
{
    public class CookieController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CookieController(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Index()
        {
            //string? cookieValueFromContext = _httpContextAccessor?.HttpContext?.Request.Cookies["key"];  
            Set("Key", "Hello from cookie", 100000);
            string? cookieValueFromReq = Get("Key");

            Remove("Key");
            return View((object)cookieValueFromReq);
        }
        public string? Get(string key)
        {
            return Request?.Cookies[key];
        }
        private void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);
            Response.Cookies.Append(key, value, option);
        }
        public IActionResult Set(string key, string value)
        {
            CookieOptions option = new CookieOptions();
            option.Expires = DateTime.Now.AddMilliseconds(10000);
            Response.Cookies.Append(key, value, option);
            return Content("Set called");
        }
        public void Remove(string key)
        {
            Response.Cookies.Delete(key);
        }
    }
}