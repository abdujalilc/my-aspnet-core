﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System.Reflection.Metadata;

namespace ScaffoldEntity.Models
{
    public class BloggingContext : DbContext
    {
        public BloggingContext(DbContextOptions<BloggingContext> options) : base(options)
        {

        }
        public DbSet<UserCode> UserCodes { get; set; }
        public DbSet<UserCodeDetail> UserCodeDetails { get; set; }

    }
}
