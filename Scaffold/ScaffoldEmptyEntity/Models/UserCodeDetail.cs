﻿public partial class UserCodeDetail
{
    public string SubCodeId { get; set; }
    public string CodeId { get; set; }
    public string Description { get; set; }
    public bool? IsDefault { get; set; }
    public int? DisplayOrder { get; set; }
    public string CreatedById { get; set; }
    public DateTime? CreatedOn { get; set; }
    public string ModifiedById { get; set; }
    public DateTime? ModifiedOn { get; set; }
    public int UserCodeDetailId { get; set; }

    public virtual UserCode Code { get; set; }
}
