﻿using System.ComponentModel.DataAnnotations;

public class UserCode
{
    public UserCode()
    {
        UserCodeDetails = new HashSet<UserCodeDetail>();
    }
    [Key]
    public int UserCodeId { get; set; }
    public string CodeId { get; set; }
    public string Description { get; set; }
    public string CreatedById { get; set; }
    public DateTime? CreatedOn { get; set; }
    public string ModifiedById { get; set; }
    public DateTime? ModifiedOn { get; set; }

    public virtual ICollection<UserCodeDetail> UserCodeDetails { get; set; }
}
