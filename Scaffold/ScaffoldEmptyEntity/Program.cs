using ScaffoldEntity.Models;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorPages();

string? con_string = "Data Source=AppData\\QuestionnaireDB.db";
builder.Services.AddSqlite<BloggingContext>(con_string);

WebApplication app = builder.Build();

app.UseStaticFiles();
app.UseRouting();
app.MapRazorPages();
app.Run();