﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScaffoldEntity.Models;

namespace ScaffoldEntity.Pages.UserCodes
{
    public class EditModel : PageModel
    {
        private readonly ScaffoldEntity.Models.BloggingContext _context;

        public EditModel(ScaffoldEntity.Models.BloggingContext context)
        {
            _context = context;
        }

        [BindProperty]
        public UserCode UserCode { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.UserCodes == null)
            {
                return NotFound();
            }

            var usercode =  await _context.UserCodes.FirstOrDefaultAsync(m => m.UserCodeId == id);
            if (usercode == null)
            {
                return NotFound();
            }
            UserCode = usercode;
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(UserCode).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserCodeExists(UserCode.UserCodeId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool UserCodeExists(int id)
        {
          return (_context.UserCodes?.Any(e => e.UserCodeId == id)).GetValueOrDefault();
        }
    }
}
