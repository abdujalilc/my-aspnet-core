﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ScaffoldEntity.Models;

namespace ScaffoldEntity.Pages.UserCodes
{
    public class CreateModel : PageModel
    {
        private readonly ScaffoldEntity.Models.BloggingContext _context;

        public CreateModel(ScaffoldEntity.Models.BloggingContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public UserCode UserCode { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid || _context.UserCodes == null || UserCode == null)
            {
                return Page();
            }

            _context.UserCodes.Add(UserCode);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
