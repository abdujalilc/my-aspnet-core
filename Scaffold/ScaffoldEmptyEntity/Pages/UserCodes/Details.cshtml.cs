﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ScaffoldEntity.Models;

namespace ScaffoldEntity.Pages.UserCodes
{
    public class DetailsModel : PageModel
    {
        private readonly ScaffoldEntity.Models.BloggingContext _context;

        public DetailsModel(ScaffoldEntity.Models.BloggingContext context)
        {
            _context = context;
        }

      public UserCode UserCode { get; set; } = default!; 

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.UserCodes == null)
            {
                return NotFound();
            }

            var usercode = await _context.UserCodes.FirstOrDefaultAsync(m => m.UserCodeId == id);
            if (usercode == null)
            {
                return NotFound();
            }
            else 
            {
                UserCode = usercode;
            }
            return Page();
        }
    }
}
