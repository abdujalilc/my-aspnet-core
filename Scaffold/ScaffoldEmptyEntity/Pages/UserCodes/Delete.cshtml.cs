﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ScaffoldEntity.Models;

namespace ScaffoldEntity.Pages.UserCodes
{
    public class DeleteModel : PageModel
    {
        private readonly ScaffoldEntity.Models.BloggingContext _context;

        public DeleteModel(ScaffoldEntity.Models.BloggingContext context)
        {
            _context = context;
        }

        [BindProperty]
      public UserCode UserCode { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.UserCodes == null)
            {
                return NotFound();
            }

            var usercode = await _context.UserCodes.FirstOrDefaultAsync(m => m.UserCodeId == id);

            if (usercode == null)
            {
                return NotFound();
            }
            else 
            {
                UserCode = usercode;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null || _context.UserCodes == null)
            {
                return NotFound();
            }
            var usercode = await _context.UserCodes.FindAsync(id);

            if (usercode != null)
            {
                UserCode = usercode;
                _context.UserCodes.Remove(UserCode);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
