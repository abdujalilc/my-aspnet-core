﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ScaffoldEntity.Models;

namespace ScaffoldEntity.Pages.UserCodeDetails
{
    public class CreateModel : PageModel
    {
        private readonly ScaffoldEntity.Models.BloggingContext _context;

        public CreateModel(ScaffoldEntity.Models.BloggingContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public UserCodeDetail UserCodeDetail { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid || _context.UserCodeDetails == null || UserCodeDetail == null)
            {
                return Page();
            }

            _context.UserCodeDetails.Add(UserCodeDetail);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
