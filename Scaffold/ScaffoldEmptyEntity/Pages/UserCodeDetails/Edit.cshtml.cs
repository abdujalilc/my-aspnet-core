﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScaffoldEntity.Models;

namespace ScaffoldEntity.Pages.UserCodeDetails
{
    public class EditModel : PageModel
    {
        private readonly ScaffoldEntity.Models.BloggingContext _context;

        public EditModel(ScaffoldEntity.Models.BloggingContext context)
        {
            _context = context;
        }

        [BindProperty]
        public UserCodeDetail UserCodeDetail { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.UserCodeDetails == null)
            {
                return NotFound();
            }

            var usercodedetail =  await _context.UserCodeDetails.FirstOrDefaultAsync(m => m.UserCodeDetailId == id);
            if (usercodedetail == null)
            {
                return NotFound();
            }
            UserCodeDetail = usercodedetail;
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(UserCodeDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserCodeDetailExists(UserCodeDetail.UserCodeDetailId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool UserCodeDetailExists(int id)
        {
          return (_context.UserCodeDetails?.Any(e => e.UserCodeDetailId == id)).GetValueOrDefault();
        }
    }
}
