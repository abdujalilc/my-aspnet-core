﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ScaffoldEntity.Models;

namespace ScaffoldEntity.Pages.UserCodeDetails
{
    public class DetailsModel : PageModel
    {
        private readonly ScaffoldEntity.Models.BloggingContext _context;

        public DetailsModel(ScaffoldEntity.Models.BloggingContext context)
        {
            _context = context;
        }

      public UserCodeDetail UserCodeDetail { get; set; } = default!; 

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.UserCodeDetails == null)
            {
                return NotFound();
            }

            var usercodedetail = await _context.UserCodeDetails.FirstOrDefaultAsync(m => m.UserCodeDetailId == id);
            if (usercodedetail == null)
            {
                return NotFound();
            }
            else 
            {
                UserCodeDetail = usercodedetail;
            }
            return Page();
        }
    }
}
