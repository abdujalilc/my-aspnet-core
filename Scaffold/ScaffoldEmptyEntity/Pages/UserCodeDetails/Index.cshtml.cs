﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ScaffoldEntity.Models;

namespace ScaffoldEntity.Pages.UserCodeDetails
{
    public class IndexModel : PageModel
    {
        private readonly ScaffoldEntity.Models.BloggingContext _context;

        public IndexModel(ScaffoldEntity.Models.BloggingContext context)
        {
            _context = context;
        }

        public IList<UserCodeDetail> UserCodeDetail { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.UserCodeDetails != null)
            {
                UserCodeDetail = await _context.UserCodeDetails.ToListAsync();
            }
        }
    }
}
