using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.Repository;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("Todos") ?? "Data Source=Todos.db";

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddDbContext<TodoDbContext>(options => 
{ 
    options.UseSqlite(connectionString)
    .EnableSensitiveDataLogging()
    .LogTo(x=>Console.WriteLine(x),LogLevel.Debug); 
});

builder.Services.AddScoped<GenericRepository<Todo>>();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = builder.Environment.ApplicationName, Version = "v1" });
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{builder.Environment.ApplicationName} v1"));
}

app.MapFallback(() => Results.Redirect("/swagger"));

app.MapGet("/test", () => "Bu test");

app.MapGet("/todos", async (GenericRepository<Todo> db) =>
{
    var result = await db.GetAll();
    return result.ToList();
});

app.MapGet("/todos/{id}", async (GenericRepository<Todo> db, int id) =>
{
    return await db.GetById(id) switch
    {
        Todo todo => Results.Ok(todo),
        null => Results.NotFound(),
    };
});

app.MapPost("/todos", async (GenericRepository<Todo> db, Todo todo) =>
{
    await db.Insert(todo);
    await db.Save();

    return Results.Created($"/todo/{todo.Id}", todo);
});

app.MapPut("/todos/{id}", async (GenericRepository<Todo> db, int id, Todo todo) =>
{
    if (id != todo.Id)
    {
        return Results.BadRequest();
    }
    IEnumerable<Todo> get_all = db.GetAll().Result;
    if (!get_all.Any<Todo>(x => x.Id == id))
    {
        return Results.NotFound();
    }    
    await db.Update(todo);
    db.Save();

    return Results.Ok();
});


app.MapDelete("/todos/{id}", async (GenericRepository<Todo> db, int id) =>
{
    var todo = await db.GetById(id);
    if (todo is null)
    {
        return Results.NotFound();
    }

    await db.Delete(todo.Id);
    await db.Save();

    return Results.Ok();
});

app.Run();
