﻿using Microsoft.AspNetCore.Mvc;

namespace razor_pages.Models
{
    public class PersonModel
    {
        [BindProperty]
        public string FirstName { get; set; }

        [BindProperty]
        public string LastName { get; set; }
    }
}
