﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace razor_pages.Models
{
    public enum RatingEnum
    {
        Unrated,
        [Display(Name = "1 Star")]
        OneStar,
        [Display(Name = "2 Star")]
        TwoStar,
        [Display(Name = "3 Star")]
        ThreeStar,
        [Display(Name = "4 Star")]
        FourStar,
        [Display(Name = "5 Star")]
        FiveStar
    }
}
