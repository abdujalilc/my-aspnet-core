using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using razor_pages.Models;

namespace razor_pages.Pages.Person
{
    public class IndexModel : PageModel
    {
        public string Name { get; set; }

        public void OnGet()
        {
        }

        public void OnPostSubmit(PersonModel person)
        {
            Name = string.Format("Name: {0} {1}", person.FirstName, person.LastName);
        }
    }
}
