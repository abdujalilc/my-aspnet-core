using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace razor_pages.Pages.Examples2
{
    public class ViewDataAttributeModel : PageModel
    {
        [ViewData]
        public string Message { get; set; }
        public void OnGet()
        {
            Message = "Hello World";
        }   
    }
}
