using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Cryptography;

namespace razor_pages.Pages.examples7
{
    public class return_json_pagemodelModel : PageModel
    {
        public void OnGet()
        {

        }
        public async Task<JsonResult> OnGetCarList1()
        {
            return new JsonResult(new { test = "Jack" });
        }
        public async Task<JsonResult> OnPostCarList2([FromForm] string test)
        {
            return new JsonResult(new { test = "Pack" });
        }
    }
}
