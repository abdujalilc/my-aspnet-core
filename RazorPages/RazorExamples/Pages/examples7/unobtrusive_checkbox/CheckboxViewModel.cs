﻿namespace razor_pages.Pages.examples7.unobtrusive_checkbox
{
    public class CourseVM
    {
        public int Id { get; set; }
        public string LabelName { get; set; }
        public bool IsChecked { get; set; }
    }
}
