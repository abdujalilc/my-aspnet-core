﻿namespace razor_pages.Pages.examples7.unobtrusive_checkbox
{
    public static class Repository
    {
        public static IList<CourseVM> GetCourses()
        {
            return new List<CourseVM>
            {
                new CourseVM
                {
                    Id = 1,
                    LabelName = "Physics",
                    IsChecked = true
                },
                new CourseVM
                {
                    Id = 2,
                    LabelName = "Chemistry",
                    IsChecked = false
                },
                new CourseVM
                {
                    Id = 3,
                    LabelName = "Mathematics",
                    IsChecked = true
                },
                new CourseVM
                {
                    Id = 4,
                    LabelName = "Biology",
                    IsChecked = false
                },
            };
        }
    }
}
