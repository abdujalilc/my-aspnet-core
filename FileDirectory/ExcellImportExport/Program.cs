using ExcellImportExport.EFContext;
using ExcellImportExport.Repository;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllersWithViews();
string? con_string = "Data Source=AppData\\database.db";
builder.Services.AddSqlite<DatabaseContext>(con_string);
builder.Services.AddRazorPages().AddRazorRuntimeCompilation();
builder.Services.AddTransient<IReporting, ReportingConcrete>();
builder.Services.AddScoped<IExportCustomer, ExportCust>();
var app = builder.Build();

app.UseStaticFiles();
app.UseRouting();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
