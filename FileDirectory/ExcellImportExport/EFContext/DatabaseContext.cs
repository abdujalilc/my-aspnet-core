﻿using ExcellImportExport.Models;
using Microsoft.EntityFrameworkCore;

namespace ExcellImportExport.EFContext
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }
        public DbSet<UserMasterModel> UserMasters { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
    }
}
