﻿using System.Data;

namespace ExcellImportExport.Repository
{
    public interface IExportCustomer
    {
        DataSet ExportCustomerDataTable();
        DataTable ExportCustomer();
    }
}
