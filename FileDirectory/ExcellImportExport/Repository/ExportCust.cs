﻿using System.Data.SqlClient;
using System.Data;
using ExcellImportExport.EFContext;
using Microsoft.EntityFrameworkCore;
using System.Data.SQLite;

namespace ExcellImportExport.Repository
{
    public class ExportCust : IExportCustomer
    {
        private readonly DatabaseContext _databaseContext;

        public ExportCust(DatabaseContext _databaseContext)
        {
            this._databaseContext = _databaseContext;
        }
        public DataTable ExportCustomer()
        {
            DataTable Custdatatable = ExportCustomerDataTable().Tables[0];
            return Custdatatable;

        }
        public DataSet ExportCustomerDataTable()
        {
            DataSet ds = new DataSet();

            string getcustomer = "SELECT * FROM Customers";
            using (SQLiteConnection scon = new SQLiteConnection(_databaseContext.Database.GetConnectionString()))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(getcustomer))
                {
                    cmd.Connection = scon;
                    using (SQLiteDataAdapter sqlAdapter = new SQLiteDataAdapter(cmd))
                    {
                        sqlAdapter.Fill(ds);

                    }
                }
            }
            return ds;
        }
    }
}
