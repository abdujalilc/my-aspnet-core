﻿using ExcellImportExport.Models;

namespace ExcellImportExport.Repository
{
    public interface IReporting
    {
        List<UserMasterViewModel> GetUserwiseReport();
    }
}
