﻿using ClosedXML.Excel;
using ExcellImportExport.EFContext;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace ExcellImportExport.Controllers
{
    public class HomeController : Controller
    {
        private readonly DatabaseContext _databaseContext;
        public HomeController(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
        // GET: Home
        public ActionResult Index()
        {
            return View(from customer in _databaseContext.Customers.Take(10)
                        select customer);
        }

        [HttpPost]
        public FileResult Export()
        {
            DataTable dt = new DataTable("Grid");
            dt.Columns.AddRange(new DataColumn[4] {
                new DataColumn("CustomerId"),
                new DataColumn("LastName"),
                new DataColumn("FirstName"),
                new DataColumn("Job")
            });

            var customers = from customer in _databaseContext.Customers.Take(10)
                            select customer;

            foreach (var customer in customers)
            {
                dt.Rows.Add(customer.id, customer.lastName, customer.firstName, customer.job);
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Grid.xlsx");
                }
            }
        }
    }
}
