﻿using ClosedXML.Excel;
using ExcellImportExport.Repository;
using Microsoft.AspNetCore.Mvc;

namespace ExcellImportExport.Controllers
{
    public class ExportController : Controller
    {

        private readonly IExportCustomer _customer;
        public ExportController(IExportCustomer customer)
        {
            _customer = customer;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult ExporttoExcel()
        {
            var arraylist = _customer.ExportCustomer();

            using (XLWorkbook xl = new XLWorkbook())
            {
                xl.Worksheets.Add(arraylist);

                using (MemoryStream mstream = new MemoryStream())
                {
                    xl.SaveAs(mstream);
                    return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Customer.xlsx");
                }
            }
        }
    }
}
