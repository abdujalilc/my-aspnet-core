﻿using System.IO.Compression;

string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;

string startPath = projectDirectory + @"\files\forStart\";
string zipPath = projectDirectory + @"\files\forZip\result.zip";
string extractPath = projectDirectory + @"\files\forUnzip\extract\";

ZipFile.CreateFromDirectory(startPath, zipPath);

ZipFile.ExtractToDirectory(zipPath, extractPath);

Console.ReadLine();