﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml;

namespace WordOperations.Services
{
    public class Report
    {
        private Body body;
        public Report()
        {
            body = new Body();
        }
        public void AddParagraph(string content)
        {
            Paragraph paragraph = GetParagraph(content);
            body.Append(paragraph);
        }

        public void AddTable<T>(List<T[]> content, string width = "500") // List<string[]>
        {
            int rowsCount = content.Count;
            int columnsCount = content.First().Length;
            Table table = new Table();
            TableProperties borders = GetTableBorder();
            table.Append(borders);

            for (int i = 0; i < rowsCount; i++)
            {
                TableRow row = new TableRow();
                for (int j = 0; j < columnsCount; j++)
                {
                    TableCell cell = new TableCell();
                    cell.AppendChild(new TableCellWidth() { Width = $"{width}", Type = TableWidthUnitValues.Pct });

                    Paragraph value = GetParagraph(content[i][j].ToString());
                    cell.Append(value);
                    row.Append(cell);
                }
                table.Append(row);
            }
            body.Append(table);
        }

        public void AddParagraph(params Run[] runs)
        {
            Paragraph paragraph = new Paragraph();
            foreach (Run run in runs)
            {
                paragraph.Append(run);
            }
            body.Append(paragraph);
        }

        public static Run GetRun(string content, VerticalPositionValues verticalPosition = VerticalPositionValues.Baseline)
        {
            Run run = new Run(
                new RunProperties(
                    new VerticalTextAlignment { Val = verticalPosition }),
                    new Text(content) { Space = SpaceProcessingModeValues.Preserve }
                );
            return run;
        }
        private Paragraph GetParagraph(string content, VerticalPositionValues verticalPosition = VerticalPositionValues.Baseline)
        {
            Paragraph paragraph = new Paragraph();
            Run run = new Run(new RunProperties(new VerticalTextAlignment { Val = verticalPosition }));
            Text text = new Text(content) { Space = SpaceProcessingModeValues.Preserve };
            run.Append(text);
            paragraph.Append(run);
            return paragraph;
        }
        private TableProperties GetTableBorder()
        {
            TableProperties properties = new TableProperties();
            TableBorders borders = new TableBorders();
            borders.Append(new LeftBorder() { Val = new EnumValue<BorderValues>(BorderValues.Thick), Color = "#000000", });
            borders.Append(new TopBorder() { Val = new EnumValue<BorderValues>(BorderValues.Thick), Color = "#000000", });
            borders.Append(new RightBorder() { Val = new EnumValue<BorderValues>(BorderValues.Thick), Color = "#000000", });
            borders.Append(new BottomBorder() { Val = new EnumValue<BorderValues>(BorderValues.Thick), Color = "#000000", });
            borders.Append(new InsideHorizontalBorder() { Val = new EnumValue<BorderValues>(BorderValues.Thick), Color = "#000000", });
            borders.Append(new InsideVerticalBorder() { Val = new EnumValue<BorderValues>(BorderValues.Thick), Color = "#000000", });
            properties.Append(borders);
            return properties;
        }
        public void SaveDocument(string path)
        {
            using (WordprocessingDocument wordDocument = WordprocessingDocument.Create(path, WordprocessingDocumentType.Document, true))
            {
                MainDocumentPart mainPart = wordDocument.AddMainDocumentPart();
                mainPart.Document = new Document();
                mainPart.Document.Body = body;
                mainPart.Document.Save();
                wordDocument.Save();
            }
        }
    }
}
