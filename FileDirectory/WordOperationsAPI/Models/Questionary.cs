﻿namespace WordOperations.Models
{
    public class Questionary
    {
        public string? Name { get; set; }
        public string? LastName { get; set; }
        public int DocumentNumber { get; set; }
        public string? Nationality { get; set; }
        public string? Post { get; set; }
        public string? Mail { get; set; }
        public string? Phone { get; set; }
        public string? Sex { get; set; }
        public string? Home { get; set; }
        public DateTime DOB { get; set; }
    }
}
