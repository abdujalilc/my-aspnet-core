﻿namespace WordOperations.Models
{
    public class Mock
    {
        public static IEnumerable<Questionary> GetCuestionariosMock()
        {
            List<Questionary> questionaries = new List<Questionary>
            {
                new Questionary
                {
                    Name = "Albert",
                    LastName = "Einstain",
                    Post = "Developer .NET",
                    Mail = "Albert@gmail.com",
                    DOB = new DateTime(1997,8,27),
                    Home = "Germany #1435",
                    Nationality = "Chinese",
                    DocumentNumber = 12,
                    Sex = "M",
                    Phone = "3124562311"
                },
                new Questionary
                {
                    Name = "Leo",
                    LastName = "Tolstoi",
                    Post = "Developer JS",
                    Mail = "Leo@gmail.com",
                    DOB = new DateTime(1996,3,25),
                    Home = "Peterburg #1254",
                    Nationality = "Russian",
                    DocumentNumber = 15,
                    Sex = "M",
                    Phone = "3124561123"
                },
            };

            return questionaries;
        }
    }
}
