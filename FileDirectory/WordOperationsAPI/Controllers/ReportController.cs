﻿using Microsoft.AspNetCore.Mvc;
using WordOperations.Services;

namespace WordOperations.Controllers
{
    public class ReportController : Controller
    {
        [HttpGet("GenerateRandomReport")]
        public IActionResult GenerateRandomReport()
        {
            string path = @"C:\Example1.docx";
            List<string[]> table = GetRandomTable();

            //creates new body
            Report report = new Report();

            report.AddParagraph("This is the first paragraph of the document");
            report.AddParagraph("This is the second paragraph of the document");

            report.AddTable(table);
            report.SaveDocument(path);

            return Ok("Success");
        }
        private List<string[]> GetRandomTable(byte rowsCount = 5, byte columnsCount = 7, double maxValue = 25)
        {
            List<string[]> table = new List<string[]>();
            Random random = new Random();

            for (int i = 0; i < rowsCount; i++)
            {
                List<string> row = new List<string>();
                for (int j = 0; j < columnsCount; j++)
                {
                    row.Add($"{Math.Round(random.NextDouble() * 10 * maxValue, 2)}");
                }
                table.Add(row.ToArray());
            }
            return table;
        }
    }
}
