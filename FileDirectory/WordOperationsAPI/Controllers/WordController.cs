﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Mvc;
using WordOperations.Models;
using Text = DocumentFormat.OpenXml.Wordprocessing.Text;
using DocumentFormat.OpenXml;

namespace WordOperations.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WordController : ControllerBase
    {
        IHostEnvironment _env;
        public WordController(IHostEnvironment env)
        {
            _env = env;
        }
        /// <summary>
        /// Base code to add rows to a table in word.
        /// </summary>
        /// <returns></returns>
        [HttpGet("AddingRowsInTable")]
        public IActionResult AddingRowsInTable()
        {
            string contentRootPath=_env.ContentRootPath;
            string path = contentRootPath+@"\Files\Questionary.docx";
            byte[] byteArray = System.IO.File.ReadAllBytes(path);

            IEnumerable<Questionary> questionaries = Mock.GetCuestionariosMock();

            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    Body body = wordDoc.MainDocumentPart.Document.Body;
                    //IEnumerable<Table> table = body.Descendants<Table>();
                    BookmarkStart bookmark = body.Descendants<BookmarkStart>().FirstOrDefault(bm => bm.Name.Equals("Members"));
                    OpenXmlElement digForTable = bookmark.Parent;

                    while (!(digForTable is Table))
                    {
                        digForTable = digForTable.Parent;
                    }

                    List<TableRow> rows = digForTable.Descendants<TableRow>().ToList();
                    //remember you have a header, so keep row 1, clone row 2 (our template for dynamic entry)
                    TableRow myRow = (TableRow)rows.Last().Clone();
                    //remove it after cloning.
                    rows.Last().Remove();

                    foreach (var questionary in questionaries)
                    {
                        //clone our "reference row"
                        TableRow rowToInsert = (TableRow)myRow.Clone();
                        //get list of cells
                        List<TableCell> listOfCellsInRow = rowToInsert.Descendants<TableCell>().ToList();
                        //just replace every bit of text in cells

                        listOfCellsInRow[0].Descendants<Text>().FirstOrDefault().Text = questionary.Name;
                        listOfCellsInRow[1].Descendants<Text>().FirstOrDefault().Text = questionary.LastName;
                        listOfCellsInRow[2].Descendants<Text>().FirstOrDefault().Text = questionary.DocumentNumber.ToString();
                        listOfCellsInRow[3].Descendants<Text>().FirstOrDefault().Text = questionary.Nationality;
                        listOfCellsInRow[4].Descendants<Text>().FirstOrDefault().Text = questionary.Post;
                        listOfCellsInRow[5].Descendants<Text>().FirstOrDefault().Text = questionary.Mail;
                        listOfCellsInRow[6].Descendants<Text>().FirstOrDefault().Text = questionary.Post;
                        listOfCellsInRow[7].Descendants<Text>().FirstOrDefault().Text = questionary.Sex;
                        listOfCellsInRow[8].Descendants<Text>().FirstOrDefault().Text = questionary.Home;
                        listOfCellsInRow[9].Descendants<Text>().FirstOrDefault().Text = questionary.DOB.ToShortDateString();

                        digForTable.Descendants<TableRow>().Last().InsertAfterSelf(rowToInsert);
                    }

                    wordDoc.MainDocumentPart.Document.Save(); // won't update the original file
                }
                // Save the file with the new name
                stream.Position = 0;
                string filename = $"NewQuestionary.docx";
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
            }
        }

        /// <summary>
        /// Replace one string with another throughout the word.
        /// </summary>
        /// <returns></returns>
        [HttpGet("ReplaceText")]
        public IActionResult ReplaceText()
        {
            string contentRootPath = _env.ContentRootPath;
            string path = contentRootPath + @"\Files\Questionary.docx";
            byte[] byteArray = System.IO.File.ReadAllBytes(path);
            var myObj = new { FullName = "Humpty Alexander Dumpty", DOB = "27/08/1997" };
            using (var stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, byteArray.Length);
                using (var wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    Body body = wordDoc.MainDocumentPart.Document.Body;
                    var old = body.InnerXml.ToString();
                    body.InnerXml = body.InnerXml.Replace("_SUPPLIER", myObj.FullName);
                    wordDoc.MainDocumentPart.Document.Save(); // won't update the original file
                }
                // Save the file with the new name
                stream.Position = 0;
                string filename = $"{myObj.FullName}.docx";
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
            }
        }

        /// <summary>
        /// Base code to insert an 'X' in a checkbox
        /// </summary>
        /// <returns></returns>
        [HttpGet("ModifyCheckbox")]
        public IActionResult ModifyCheckbox()
        {
            string contentRootPath = _env.ContentRootPath;
            string path = contentRootPath + @"\Files\Questionary.docx";
            byte[] byteArray = System.IO.File.ReadAllBytes(path);

            var myObj = new { FullName = "Nicola Tesla", DOB = "27/08/1981" };
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, byteArray.Length);
                using (var wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    Body body = wordDoc.MainDocumentPart.Document.Body;
                    BookmarkStart bookmark = body.Descendants<BookmarkStart>().FirstOrDefault(bm => bm.Name.Equals(true ? "SI_5_1" : "NO_5_1"));

                    ParagraphProperties paragraphProperties = new ParagraphProperties { Justification = new Justification { Val = JustificationValues.Center } };
                    bookmark.Parent.Append(paragraphProperties);

                    Run run = new Run();
                    RunProperties runProperties = new RunProperties();
                    runProperties.Bold = new Bold();

                    run.Append(runProperties);
                    run.Append(new Text("X"));

                    bookmark.Parent.Append(run);

                    wordDoc.MainDocumentPart.Document.Save(); // won't update the original file
                }
                // Save the file with the new name
                stream.Position = 0;
                string filename = "CheckboxQuestionary.docx";
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
            }
        }

        /// <summary>
        /// Base code to insert text in a bookmark
        /// </summary>
        /// <returns></returns>
        [HttpGet("InsertTextBookMark")]
        public IActionResult InsertTextBookMark()
        {
            string contentRootPath = _env.ContentRootPath;
            string path = contentRootPath + @"\Files\Questionary.docx";
            byte[] byteArray = System.IO.File.ReadAllBytes(path);

            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, byteArray.Length);
                using (var wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    Body body = wordDoc.MainDocumentPart.Document.Body;
                    BookmarkStart bookmark = body.Descendants<BookmarkStart>().FirstOrDefault(bm => bm.Name.Equals("Members"));

                    ParagraphProperties paragraphProperties = new ParagraphProperties { Justification = new Justification { Val = JustificationValues.Left } };
                    bookmark.Parent.Append(paragraphProperties);

                    Run run = new Run();
                    RunProperties runProperties = new RunProperties();
                    runProperties.Bold = new Bold();

                    run.Append(runProperties);
                    run.Append(new Text("3121589645"));

                    bookmark.Parent.Append(run);

                    wordDoc.MainDocumentPart.Document.Save(); // won't update the original file
                }
                // Save the file with the new name
                stream.Position = 0;
                string filename = "TextBookMarkQuestionary.docx";
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
            }
        }


        /// <summary>
        /// Base code to insert a text in front of another text with a bookmark
        /// </summary>
        /// <returns></returns>
        [HttpGet("InsertTextAfterAnotherTextBookMark")]
        public IActionResult InsertTextAfterAnotherTextBookMark()
        {
            string contentRootPath = _env.ContentRootPath;
            string path = contentRootPath + @"\Files\Questionary.docx";
            byte[] byteArray = System.IO.File.ReadAllBytes(path);

            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    Body body = wordDoc.MainDocumentPart.Document.Body;
                    BookmarkStart bookmark = body.Descendants<BookmarkStart>().FirstOrDefault(bm => bm.Name.Equals("Members"));

                    ParagraphProperties paragraphProperties = new ParagraphProperties { Justification = new Justification { Val = JustificationValues.Left } };
                    bookmark.Parent.Append(paragraphProperties);

                    Run run = new Run();
                    RunProperties runProperties = new RunProperties();
                    runProperties.Bold = new Bold();

                    run.Append(runProperties);
                    run.Append(new Text("Grasshopper #1435"));

                    bookmark.Parent.Append(run);

                    wordDoc.MainDocumentPart.Document.Save(); // won't update the original file
                }
                // Save the file with the new name
                stream.Position = 0;
                string filename = "TextAfterAnotherTextBookMarkQuestionary.docx";
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
            }
        }

        /// <summary>
        /// Base code to insert rows into an internal table
        /// </summary>
        /// <returns></returns>
        [HttpGet("AddingRowsInInnerTable")]
        public IActionResult AddingRowsInInnerTable()
        {
            string contentRootPath = _env.ContentRootPath;
            string path = contentRootPath + @"\Files\Questionary.docx";
            byte[] byteArray = System.IO.File.ReadAllBytes(path);

            IEnumerable<Questionary> cuestionarios = Mock.GetCuestionariosMock();

            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    Body body = wordDoc.MainDocumentPart.Document.Body;
                    IEnumerable<Table> table = body.Descendants<Table>();
                    BookmarkStart bookmark = body.Descendants<BookmarkStart>().FirstOrDefault(bm => bm.Name.Equals("Members"));
                    OpenXmlElement digForRow = bookmark.Parent;

                    while (!(digForRow is TableRow))
                    {
                        digForRow = digForRow.Parent;
                    }

                    //remember you have a header, so keep row 1, clone row 2 (our template for dynamic entry)
                    TableRow myRow = (TableRow)digForRow.Clone();

                    foreach (var cuestionario in cuestionarios)
                    {
                        //clone our "reference row"
                        TableRow rowToInsert = (TableRow)myRow.Clone();
                        //get list of cells
                        List<TableCell> listOfCellsInRow = rowToInsert.Descendants<TableCell>().ToList();
                        //just replace every bit of text in cells

                        listOfCellsInRow[0].Descendants<Text>().FirstOrDefault().Text = cuestionario.Name;
                        listOfCellsInRow[1].Descendants<Text>().FirstOrDefault().Text = cuestionario.LastName;
                        listOfCellsInRow[2].Descendants<Text>().FirstOrDefault().Text = cuestionario.DocumentNumber.ToString();
                        listOfCellsInRow[3].Descendants<Text>().FirstOrDefault().Text = cuestionario.Nationality;
                        listOfCellsInRow[4].Descendants<Text>().FirstOrDefault().Text = cuestionario.Post;
                        listOfCellsInRow[5].Descendants<Text>().FirstOrDefault().Text = cuestionario.Mail;
                        listOfCellsInRow[6].Descendants<Text>().FirstOrDefault().Text = cuestionario.Phone;
                        listOfCellsInRow[7].Descendants<Text>().FirstOrDefault().Text = cuestionario.Sex;
                        listOfCellsInRow[8].Descendants<Text>().FirstOrDefault().Text = cuestionario.Home;
                        listOfCellsInRow[9].Descendants<Text>().FirstOrDefault().Text = cuestionario.DOB.ToShortDateString();

                        digForRow.InsertAfterSelf(rowToInsert);
                    }

                    digForRow.Remove();
                    wordDoc.MainDocumentPart.Document.Save(); // won't update the original file
                }
                // Save the file with the new name
                stream.Position = 0;
                string filename = $"EditInnerTable.docx";
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
            }
        }
    }
}
