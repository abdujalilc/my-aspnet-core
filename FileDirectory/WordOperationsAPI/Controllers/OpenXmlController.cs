﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Mvc;

namespace WordOperations.Controllers
{
    public class OpenXmlController : Controller
    {
        IHostEnvironment _env;
        public OpenXmlController(IHostEnvironment env)
        {
            _env = env;
        }
        [HttpGet("CheckInnerXml")]
        public IActionResult CheckInnerXml(string filename="image")
        {
            string poli = $"{filename}";
            string contentRootPath = _env.ContentRootPath;
            string path = contentRootPath + $@"\Files\InnerXml\{filename}.docx";
            byte[] byteArray = System.IO.File.ReadAllBytes(path);

            using (var stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, byteArray.Length);
                using (var wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    Body body = wordDoc.MainDocumentPart.Document.Body;
                    var old = body.InnerXml.ToString();
                }
                return Json("Success");
            }
        }
    }
}
