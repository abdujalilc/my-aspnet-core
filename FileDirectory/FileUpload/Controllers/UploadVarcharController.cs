﻿using FileUpload.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FileUpload.Controllers
{
    public class UploadVarcharController : Controller
    {
        private ApplicationDbContext entities { get; set; }
        public UploadVarcharController(ApplicationDbContext entities)
        {
            this.entities = entities;
        }
        // GET: Home
        public ActionResult Index()
        {
            return View(entities.tblFiles2.ToList());
        }

        [HttpPost]
        public ActionResult Index(IFormFile postedFile)
        {
            byte[] bytes;
            using (MemoryStream ms = new MemoryStream())
            {
                postedFile.CopyTo(ms);
                bytes = ms.ToArray();

            }
            entities.tblFiles2.Add(new tblFile
            {
                Name = Path.GetFileName(postedFile.FileName),
                ContentType = postedFile.ContentType,
                Data = bytes
            });
            entities.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public FileResult DownloadFile(int? fileId)
        {
            tblFile file = entities.tblFiles2.ToList().Find(p => p.id == fileId.Value);
            return File(file.Data, file.ContentType, file.Name);
        }
    }
}
