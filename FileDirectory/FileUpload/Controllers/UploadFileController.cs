﻿using Microsoft.AspNetCore.Mvc;
using System.Data.SQLite;

public class UploadFileController : Controller
{
    private IConfiguration Configuration;
    private ApplicationDbContext applicationDbContext;

    public UploadFileController(IConfiguration _configuration, ApplicationDbContext applicationDbContext)
    {
        Configuration = _configuration;
        this.applicationDbContext = applicationDbContext;
    }

    public IActionResult Index()
    {
        var test = applicationDbContext.tblFiles.ToList();
        return View(this.GetFiles());
    }

    [HttpPost]
    public IActionResult UploadFile(IFormFile postedFile)
    {
        var test = applicationDbContext.tblFiles.ToList();
        string fileName = Path.GetFileName(postedFile.FileName);
        string contentType = postedFile.ContentType;
        using (MemoryStream ms = new MemoryStream())
        {
            postedFile.CopyTo(ms);
            string? con_string = "Data Source=AppData\\database.db";
            using (SQLiteConnection con = new SQLiteConnection(con_string))
            {
                string query = "INSERT INTO tblFiles(Name, ContentType, Data) VALUES (@Name, @ContentType, @Data)";
                using (SQLiteCommand cmd = new SQLiteCommand(query))
                {
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@Name", fileName);
                    cmd.Parameters.AddWithValue("@ContentType", contentType);
                    cmd.Parameters.AddWithValue("@Data", ms.ToArray());
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        return RedirectToAction("Index");
    }

    [HttpPost]
    public IActionResult DownloadFile(int fileId)
    {
        byte[] bytes;
        string fileName, contentType;
        string? con_string = "Data Source=AppData\\database.db";
        using (SQLiteConnection con = new SQLiteConnection(con_string))
        {
            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                cmd.CommandText = "SELECT Name, Data, ContentType FROM tblFiles WHERE Id=@Id";
                cmd.Parameters.AddWithValue("@Id", fileId);
                cmd.Connection = con;
                con.Open();
                using (SQLiteDataReader sdr = cmd.ExecuteReader())
                {
                    sdr.Read();
                    bytes = (byte[])sdr["Data"];
                    contentType = sdr["ContentType"].ToString();
                    fileName = sdr["Name"].ToString();
                }
                con.Close();
            }
        }

        return File(bytes, contentType, fileName);
    }

    private List<FileModel> GetFiles()
    {
        List<FileModel> files = new List<FileModel>();
        string? con_string = "Data Source=AppData\\database.db";

        using (SQLiteConnection con = new SQLiteConnection(con_string))
        {
            using (SQLiteCommand cmd = new SQLiteCommand("SELECT Id, Name FROM tblFiles"))
            {
                cmd.Connection = con;
                con.Open();
                using (SQLiteDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        files.Add(new FileModel
                        {
                            Id = Convert.ToInt32(sdr["Id"]),
                            Name = sdr["Name"].ToString()
                        });
                    }
                }
                con.Close();
            }
        }
        return files;
    }
}