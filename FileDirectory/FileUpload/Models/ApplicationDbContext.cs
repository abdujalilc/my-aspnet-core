﻿using FileUpload.Models;
using Microsoft.EntityFrameworkCore;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
        Database.EnsureCreated();
    }

    public DbSet<FileModel> tblFiles { get; set; }

    public DbSet<tblFile> tblFiles2 { get; set; }
}