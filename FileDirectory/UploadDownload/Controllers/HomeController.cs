﻿using Microsoft.AspNetCore.Mvc;
using System.IO.Compression;

namespace upload_download.Controllers
{
    public class HomeController : Controller
    {
        IWebHostEnvironment webHostEnvironment;
        public HomeController(IWebHostEnvironment webHostEnvironment)
        {
            this.webHostEnvironment = webHostEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(IFormFile zip)
        {
            try
            {
                string uploadsFolder = webHostEnvironment.WebRootPath + "\\uploads\\";
                var tempFilePath = uploadsFolder +"temp\\"+ zip.FileName;
                var folderForExtract= uploadsFolder + "extract\\" + Guid.NewGuid().ToString();

                using (var stream = new FileStream(tempFilePath, FileMode.Create))
                {
                    zip.CopyTo(stream);
                }                
                ZipFile.ExtractToDirectory(tempFilePath, folderForExtract);

                if (System.IO.File.Exists(tempFilePath))
                {
                    System.IO.File.Delete(tempFilePath);
                }
                ViewBag.Result = "Success";
                return View();
            }
            catch(Exception ex)
            {
                ViewBag.Result = ex.Message;
                return View();
            }            
        }
    }
}
