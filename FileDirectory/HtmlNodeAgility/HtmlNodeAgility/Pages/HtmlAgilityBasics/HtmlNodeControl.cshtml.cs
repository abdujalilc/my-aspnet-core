using Microsoft.AspNetCore.Mvc.RazorPages;
using HtmlAgilityPack;

namespace HtmlNodeAgility.Pages.HtmlAgilityBasics
{
    public class HtmlNodeControlModel : PageModel
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public HtmlNodeControlModel(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }
        public void OnGet()
        {

        }
        public void OnPost()
        {
            try
            {
                string wwwRootPath = _webHostEnvironment.WebRootPath;
                string htmlFilePath = Path.Combine(wwwRootPath, "Templates/30/", "cple-atttendance.html");

                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.Load(htmlFilePath);

                HtmlNode participantNode = htmlDoc.DocumentNode.SelectSingleNode("//div[@id='participant-name']");
                if (participantNode != null)
                {
                    participantNode.InnerHtml = "Tom Smith";
                }
                HtmlNode eventNameNode = htmlDoc.DocumentNode.SelectSingleNode("//div[@id='event-name']");
                if (eventNameNode != null)
                {
                    eventNameNode.InnerHtml = "Some Event Changed";
                }
                // Save the modified HTML content to a file or perform further operations
                string modifiedHtml = htmlDoc.DocumentNode.OuterHtml;

                string modifiedFilePath = Path.Combine(wwwRootPath, "Certificates", "modifiedHtmlFile.html");
                if (System.IO.File.Exists(modifiedFilePath))
                    System.IO.File.Delete(modifiedFilePath);
                System.IO.File.WriteAllText(modifiedFilePath, modifiedHtml);

                TempData["msg"] = "alert('Change succesfully');";
            }
            catch (Exception ex)
            {
                TempData["msg"] = ex.Message;
            }
        }
    }
}
