var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllersWithViews();
string? con_string = "Data Source=AppData\\database.db";
//builder.Services.AddSqlite<DatabaseContext>(con_string);
builder.Services.AddRazorPages().AddRazorRuntimeCompilation();
var app = builder.Build();

app.UseStaticFiles();
app.UseRouting();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
