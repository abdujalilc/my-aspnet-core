﻿using HtmlToExcell.Extensions;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using TowerSoft.HtmlToExcel;

namespace HtmlToExcell.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult singleSheet()
        {
            string htmlString = "<table><tbody><tr><td>Cell contents</td></tr></tbody></table>";

            HtmlToExcelSettings settings = HtmlToExcelSettings.Defaults;
            settings.AutofitColumns = false;
            settings.ShowFilter = false;
            settings.ShowRowStripes = false;

            byte[] fileData = new WorkbookGenerator(settings).FromHtmlString(htmlString);
            return File(fileData, MimeType.xlsx, "filename.xlsx");
        }
        public IActionResult multiSheet()
        {
            string htmlString1 = "<table><tbody><tr><td>Cell contents sheet1</td></tr></tbody></table>";
            string htmlString2 = "<table><tbody><tr><td>Cell contents sheet2</td></tr></tbody></table>";

            byte[] fileData;
            using (WorkbookBuilder workbookBuilder = new WorkbookBuilder())
            {
                workbookBuilder.AddSheet("sheet1", htmlString1);
                workbookBuilder.AddSheet("sheet2", htmlString2);

                fileData = workbookBuilder.GetAsByteArray();
            }
            return File(fileData, MimeType.xlsx, "filename.xlsx");
        }
        public async Task<IActionResult> ExcelFile()
        {
            var model = "test";
            string htmlString = await this.RenderViewAsync("TestView", model, true);
            byte[] fileData = new WorkbookGenerator().FromHtmlString(htmlString);

            return File(fileData, MimeType.xlsx, "filename.xlsx");
        }
        public IActionResult TestView()
        {
            return View();
        }
    }
}
