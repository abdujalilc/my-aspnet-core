﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;

namespace ClosedXmlDocs.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public FileResult SimpleExample()
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                using var wbook = new XLWorkbook();

                var ws = wbook.Worksheets.Add("Sheet1");
                ws.Cell("A1").Value = "150";
                wbook.SaveAs(mstream);
                return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SimpleExample.xlsx");
            }
        }
        public IActionResult ExcelCell()
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                using var wbook = new XLWorkbook();

                var ws = wbook.AddWorksheet("Sheet1");

                ws.FirstCell().Value = 150;

                ws.Cell(3, 2).Value = "Hello there!";
                ws.Cell("A6").SetValue("falcon").SetActive();

                ws.Column(2).AdjustToContents();
                wbook.SaveAs(mstream);
                return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ExcelCell.xlsx");
            }
        }
        public FileResult ApplyStyle()
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                using var wbook = new XLWorkbook();

                var ws = wbook.AddWorksheet("Sheet1");

                var c1 = ws.Column("A");
                c1.Width = 25;

                var c2 = ws.Column("B");
                c2.Width = 15;

                ws.Cell("A3").Value = "an old falcon";
                ws.Cell("B2").Value = "150";
                ws.Cell("B5").Value = "Sunny day";

                ws.Cell("A3").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Cell("A3").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                ws.Cell("A3").Style.Font.Italic = true;

                ws.Cell("B2").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Cell("B5").Style.Font.FontColor = XLColor.Red;
                wbook.SaveAs(mstream);
                return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ApplyStyle.xlsx");
            }
        }
        public FileResult ExcelRanges()
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                using var wbook = new XLWorkbook();
                var ws = wbook.AddWorksheet("Sheet1");

                ws.Range("D2:E2").Style.Fill.BackgroundColor = XLColor.Gray;
                ws.Ranges("C5, F5:G8").Style.Fill.BackgroundColor = XLColor.Gray;

                var rand = new Random();
                var range = ws.Range("C10:E15");

                foreach (var cell in range.Cells())
                {
                    cell.Value = rand.Next();
                }

                ws.Column("C").AdjustToContents();
                ws.Column("D").AdjustToContents();
                ws.Column("E").AdjustToContents();

                wbook.SaveAs(mstream);
                return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ExcelRanges.xlsx");
            }
        }
        public FileResult MergeCells()
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                using var wbook = new XLWorkbook();
                var ws = wbook.AddWorksheet("Sheet1");

                ws.Cell("A1").Value = "Sunny day";
                ws.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Cell("A1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                ws.Range("A1:B2").Merge();

                wbook.SaveAs(mstream);
                return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "MergeCells.xlsx");
            }
        }
        public FileResult Sorting()
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                using var wbook = new XLWorkbook();
                var ws = wbook.AddWorksheet("Sheet1");

                var rand = new Random();
                var range = ws.Range("A1:A15");

                foreach (var cell in range.Cells())
                {
                    cell.Value = rand.Next(1, 100);
                }

                ws.Sort("A");

                wbook.SaveAs(mstream);
                return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Sorting.xlsx");
            }
        }
        public FileResult CellsUsed()
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                using var wbook = new XLWorkbook();
                var ws = wbook.AddWorksheet("Sheet1");
                ws.Cell("A1").Value = "sky";
                ws.Cell("A2").Value = "cloud";
                ws.Cell("A3").Value = "book";
                ws.Cell("A4").Value = "cup";
                ws.Cell("A5").Value = "snake";
                ws.Cell("A6").Value = "falcon";
                ws.Cell("B1").Value = "in";
                ws.Cell("B2").Value = "tool";
                ws.Cell("B3").Value = "war";
                ws.Cell("B4").Value = "snow";
                ws.Cell("B5").Value = "tree";
                ws.Cell("B6").Value = "ten";

                var n = ws.Range("A1:C10").CellsUsed().Count();
                ws.Cell("B20").Value = n;
                
                wbook.SaveAs(mstream);
                return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "CellsUsed.xlsx");
            }
        }
        public FileResult ExpressionEvaluation()
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                using var wbook = new XLWorkbook();
                var ws = wbook.AddWorksheet("Sheet1");
                ws.Cell("A1").Value = 1;
                ws.Cell("A2").Value = 2;
                ws.Cell("A3").Value = 3;
                ws.Cell("A4").Value = 4;
                ws.Cell("A5").Value = 5;
                ws.Cell("A6").Value = 6;

                var sum = ws.Evaluate("SUM(A1:A6)");
                var max = ws.Evaluate("MAX(A1:A6)");
                ws.Cell("A7").Value = sum;
                ws.Cell("A8").Value = max;

                wbook.SaveAs(mstream);
                return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ExpressionEvaluation.xlsx");
            }
        }
        public FileResult Formula()
        {
            using (MemoryStream mstream = new MemoryStream())
            {
                using var wbook = new XLWorkbook();
                var ws = wbook.AddWorksheet("Sheet1");
                ws.Cell("A1").Value = 1;
                ws.Cell("A2").Value = 2;
                ws.Cell("A3").Value = 3;
                ws.Cell("A4").Value = 4;
                ws.Cell("A5").Value = 5;
                ws.Cell("A6").Value = 6;

                ws.Cell("A8").FormulaA1 = "SUM(A1:A7)";
                ws.Cell("A8").Style.Font.Bold = true;

                wbook.SaveAs(mstream);
                return File(mstream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Formula.xlsx");
            }
        }
    }
}
