﻿namespace CheckBox.Models
{
    public class Hobby
    {
        public int HobbyId { get; set; }
        public string? HobbyName { get; set; }
        public bool IsSelected { get; set; }

    }
}
