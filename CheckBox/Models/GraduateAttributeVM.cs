﻿namespace CheckBox.Models
{
    public class EventModel
    {
        public List<GraduateAttributeVM> GraduateAttributes { get; set; }
    }
    public class GraduateAttributeVM
    {
        public string Id { get; set; }
        public bool IsSelected { get; set; }
        public string Description { get; set; }
    }
}
