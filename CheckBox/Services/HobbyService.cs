﻿using CheckBox.Models;
using System.Text.Json;
using System;

namespace CheckBox.Services
{
    public class HobbyService
    {
        private IWebHostEnvironment env;
        public HobbyService(IWebHostEnvironment env)
        {
            this.env = env;
        }
        public List<Hobby>? hobbies = new List<Hobby>();

        public List<Hobby>? GetHobbies()
        {
            using (StreamReader r = new StreamReader(env.WebRootPath+"\\json\\data.json"))
            {
                string json = r.ReadToEnd();
                hobbies = JsonSerializer.Deserialize<List<Hobby>>(json);
            }

            return hobbies;
        }
    }
}
