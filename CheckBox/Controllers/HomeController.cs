﻿using CheckBox.Models;
using CheckBox.Services;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace CheckBox.Controllers
{
    public class HomeController : Controller
    {
        private IWebHostEnvironment env;
        public HomeController(IWebHostEnvironment env)
        {
            this.env = env;
        }

        public ActionResult Index()
        {
            HobbyService hobbyService = new HobbyService(env);
            return View(hobbyService.GetHobbies());
        }

        [HttpPost]
        public ActionResult Save(List<Hobby> hobbies)
        {
            List<Hobby> _hobbies = hobbies.Select(d => new Hobby
            {
                HobbyId= d.HobbyId,
                HobbyName= d.HobbyName,
                IsSelected= d.IsSelected,
            }).ToList();

            string jsonString = JsonSerializer.Serialize(_hobbies, new JsonSerializerOptions() { WriteIndented = true });
            using (StreamWriter outputFile = new StreamWriter(env.WebRootPath+ "\\json\\data.json"))
            {
                outputFile.WriteLine(jsonString);
            }
            return RedirectToAction("Index");
        }
    }
}
