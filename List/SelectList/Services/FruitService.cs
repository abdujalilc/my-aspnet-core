﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Models;

namespace SelectList.Services
{
    public class FruitService
    {
        public static List<SelectListItem> PopulateFruits()
        {            
            List<SelectListItem> fruits = new List<FruitModel>() 
            { 
                new FruitModel() {FruitId=1,FruitName="Mango"},
                new FruitModel() {FruitId=2,FruitName="Orange"},
                new FruitModel() {FruitId=3,FruitName="Banana"},
                new FruitModel() {FruitId=4,FruitName="Apple"},
                new FruitModel() {FruitId=5,FruitName="papaya"},
            }.Select(x=>new SelectListItem { Value=x.FruitId.ToString(), Text=x.FruitName}).ToList();          

            return fruits;
        }
    }
}
