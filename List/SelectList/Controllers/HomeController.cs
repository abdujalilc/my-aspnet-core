﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Rendering=Microsoft.AspNetCore.Mvc.Rendering;
using SelectList.Services;

namespace SelectList.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            FruitViewModel fruitViewModel = new FruitViewModel();
            fruitViewModel.FruitLists = FruitService.PopulateFruits();
            return View(fruitViewModel);
        }
        [HttpPost]
        public IActionResult Index(FruitViewModel fruitViewModel)
        {
            ViewBag.Message = "Fruit Name: " + fruitViewModel.FruitName;
            ViewBag.Message += "\\nFruit Id: " + fruitViewModel.FruitId;
            return View(fruitViewModel);
        }
    }
}
