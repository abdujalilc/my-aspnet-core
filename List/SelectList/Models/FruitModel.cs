﻿namespace Models
{
    public class FruitModel
    {
        public int FruitId { get; set; }
        public string FruitName { get; set; }
    }
}
