﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace Models
{
    public class FruitViewModel
    {
        public int FruitId { get; set; }
        public string FruitName { get; set; }
        public List<SelectListItem> FruitLists { get; set; }
    }
}
