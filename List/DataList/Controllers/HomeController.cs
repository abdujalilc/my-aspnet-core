﻿using Microsoft.AspNetCore.Mvc;

namespace DataList.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult DataList()
        {
            return View(GetCountries());
        }
        private List<Country> GetCountries()
        {
            List<Country> countries = new List<Country>()
            {
                new Country(1,"USA"),
                new Country(2,"Uzb"),
                new Country(3,"India"),
                new Country(4,"Russia")
            };
            return countries;
        }
        public IActionResult GetIdByValue()
        {
            return View();
        }
    }
    public record Country(int CountryId, string? CountryName);
}
