﻿using Microsoft.AspNetCore.Mvc;

namespace view_component.Components
{
    public class UsersListViewComponent : ViewComponent
    {
        private readonly List<string> users = new()
        {
            "Tom", "Tim", "Bob", "Sam", "Alice", "Kate"
        };
        public IViewComponentResult Invoke()
        {
            int number = users.Count;
            // если передан параметр number
            if (Request.Query.ContainsKey("number"))
            {
                _ = int.TryParse(Request.Query["number"], out number);
            }

            ViewBag.Users = users.Take(number);
            ViewData["Header"] = $"Количество пользователей: {number}.";
            return View("Users", users);
        }

        private void Test()
        {
            _ = HttpContext;
            _ = ModelState;
            _ = Request;
            _ = RouteData;
            _ = Url;
            _ = User;
            _ = ViewBag;
            _ = ViewContext;
            _ = ViewComponentContext;
            _ = ViewData;
            _ = ViewContext?.HttpContext?.Request!;
        }

    }
}
