﻿using Microsoft.AspNetCore.Mvc;

namespace view_component.Controllers
{
    public class HomeController : Controller
    {
        public Task<ViewResult> Index()
        {
            return Task.FromResult(View());
        }
    }
}