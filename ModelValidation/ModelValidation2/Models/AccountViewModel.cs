﻿using System.ComponentModel.DataAnnotations;

namespace ModelValidation.Models
{
    public class AccountViewModel
    {
        [Required(ErrorMessage = "Email is required.")]
        [RegularExpression(@"^\w+([-+.']\w+)*@123g.com$", ErrorMessage = "Invalid Email.")]
        public string Email { get; set; }
    }
}
