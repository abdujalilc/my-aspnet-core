﻿using System.ComponentModel.DataAnnotations;

public class PersonModel
{
    [Display(Name = "I accept the above terms and conditions.")]
    [CheckBoxRequired(ErrorMessage = "Please accept the terms and condition.")]
    public bool TermsConditions { get; set; }
}