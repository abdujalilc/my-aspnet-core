﻿using System.ComponentModel.DataAnnotations;

namespace ModelValidation.Models.CustomValidation
{
    public class ValidEmailDomainAttribute : ValidationAttribute
    {
        private readonly string allowedDomain;

        public ValidEmailDomainAttribute(string allowedDomain)
        {
            this.allowedDomain = allowedDomain;
        }

        public override bool IsValid(object value)
        {
            string[] strings = value.ToString().Split('@');
            return strings[1].ToUpper() == allowedDomain.ToUpper();
        }
    }
    public class RegisterViewModel
    {
        [ValidEmailDomain(allowedDomain: "pragimtech.com",
            ErrorMessage = "Email domain must be pragimtech.com")]
        public string Email { get; set; }
    }
}
