﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace AutocompleteTextbox.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View(new RECaptcha());
        }

        [HttpPost]
        public JsonResult AjaxMethod([FromBody] string response)
        {
            RECaptcha recaptcha = new RECaptcha();
            string url = "https://www.google.com/recaptcha/api/siteverify?secret=" + recaptcha.Secret + "&response=" + response;
            recaptcha.Response = (new WebClient()).DownloadString(url);
            return Json(recaptcha);
        }
    }
}
