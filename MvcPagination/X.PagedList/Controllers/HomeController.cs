﻿using Microsoft.AspNetCore.Mvc;
using mvc_pagination.Models;
using System.Diagnostics;
using X.PagedList;

namespace mvc_pagination.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index(int? Page)
        {
            var page = Page ?? 1;
            StudentRepository studentRepository = new StudentRepository();
            IPagedList test_model = studentRepository.GetStudents().ToPagedList(page, 5);
            
            return View(test_model);
        }
    }

}