﻿using X.PagedList;

namespace mvc_pagination.Models
{
    public class StudentRepository
    {
        public IList<Student> GetStudents()
        {
            List<Student> students = new List<Student>()
            {
                new Student(){Id=1,Name="Tom Smith"},
                new Student(){Id=2,Name="Jack Black"},
                new Student(){Id=3,Name="Julia Jackson"},
                new Student(){Id=4,Name="William Dickson"},
                new Student(){Id=5,Name="Everly Humphries"},
                new Student(){Id=6,Name="Louie Ellwood"},
                new Student(){Id=7,Name="Maira Solomon"},
                new Student(){Id=8,Name="Isla Haley"},
                new Student(){Id=9,Name="Lemar Dale"},
                new Student(){Id=10,Name="Lilly-May Rocha"},
                new Student(){Id=11,Name="Pharrell Calhoun"},
                new Student(){Id=12,Name="Tyler-James Haworth"},
                new Student(){Id=13,Name="Cyrus Poole"},
                new Student(){Id=14,Name="Marlie Avalos"},
            };
            return students.ToList();
        }
    }
}
