﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Text.Json;
using ViewModels;

namespace PagingTagHelper.Controllers
{
    public class HomeController : Controller
    {
        public int PageSize = 4;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public HomeController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }
        public ViewResult Index(int productPage = 1)
        {
            string filePath = Path.Combine(_webHostEnvironment.WebRootPath, "products.json");
            string json = System.IO.File.ReadAllText(filePath);
            List<Product> products = JsonSerializer.Deserialize<List<Product>>(json);
            return View(new ProductsListViewModel
            {
                Products = products.OrderBy(p => p.ProductID).Skip((productPage - 1) * PageSize).Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = productPage,
                    ItemsPerPage = PageSize,
                    TotalItems = products.Count()
                }
            });
        }
    }
}
/*
string filePath = Path.Combine(_webHostEnvironment.WebRootPath, "jsonfile.json");
string json = System.IO.File.ReadAllText(filePath);
List<Product> products = JsonSerializer.Deserialize<List<Product>>(json);
return View(products); // Pass the list to the view
 */