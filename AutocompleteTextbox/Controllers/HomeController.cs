﻿using AutocompleteTextbox.Models;
using Microsoft.AspNetCore.Mvc;

namespace AutocompleteTextbox.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult AutoComplete([FromBody]string prefix)
        {
            var req = Request;
            if (prefix == null)
            {
                return Json(new { });
            }
            try
            {
                var customers = (from customer in Customers.GetCustomers()
                                 where customer.ContactName.StartsWith(prefix)
                                 select new
                                 {
                                     label = customer.ContactName,
                                     val = customer.CustomerID
                                 }).ToList();

                return Json(customers);
            }
            catch
            {
                return Json(new { });
            }
        }

        [HttpPost]
        public ActionResult Index(string CustomerName, string CustomerId)
        {
            ViewBag.Message = "CustomerName: " + CustomerName + " CustomerId: " + CustomerId;
            return View();
        }
    }
}
