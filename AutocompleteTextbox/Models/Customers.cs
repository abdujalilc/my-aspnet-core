﻿namespace AutocompleteTextbox.Models
{
    public class Customers
    {
        static List<Customer> customers = new List<Customer>();
        public static List<Customer> GetCustomers()
        {
            return customers = new List<Customer>()
            {
                new Customer() { CustomerID=1, ContactName="Lev Tolstoy"},
                new Customer() { CustomerID=2, ContactName="Rob Tagor"},
                new Customer() { CustomerID=3, ContactName="Red Shield"},
                new Customer() { CustomerID=4, ContactName="Henry Ford"},
                new Customer() { CustomerID=5, ContactName="John Rockfeller"},
                new Customer() { CustomerID=6, ContactName="J.P. Morgan"},
                new Customer() { CustomerID=7, ContactName="Andrew Carnegie"}
            };
        }
    }
    public class Customer
    {
        public int CustomerID { get; set; }
        public string ContactName { get; set; }
    }
}
