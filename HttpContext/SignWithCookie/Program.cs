using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.FileProviders;
using SignWithCookie.Services;
using System.Net;
using System.Security.Claims;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorPages();
builder.Services.AddRazorPages(options =>
{
    options.Conventions.AuthorizePage("/Index");
    options.Conventions.AuthorizeFolder("/Private");
    options.Conventions.AllowAnonymousToPage("/AllowedPage");
    options.Conventions.AllowAnonymousToPage("/Public/Login");
    options.Conventions.AllowAnonymousToFolder("/Public");
});
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
        .AddCookie(options =>
        {
            options.LoginPath = "/public/login";
            options.Cookie.Name = "my_app_auth_cookie";
        });
var app = builder.Build();

app.UseStaticFiles();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.ProtectLogoImages();
app.MapRazorPages();
app.Run();
