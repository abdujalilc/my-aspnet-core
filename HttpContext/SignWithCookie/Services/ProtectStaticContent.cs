﻿using Microsoft.Extensions.FileProviders;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Claims;

namespace SignWithCookie.Services
{
    public class ProtectStaticContent
    {
        private readonly RequestDelegate _next;

        public ProtectStaticContent(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.Path.StartsWithSegments("/images", StringComparison.CurrentCultureIgnoreCase))
            {
                httpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                return;
            }
            await _next(httpContext);
        }
        
    }
    
}
