﻿using Microsoft.Extensions.FileProviders;
using System.Net;
using System.Security.Claims;

namespace SignWithCookie.Services
{
    public static class ProtectStaticFiles
    {
        public static void ProtectLogoImages(this IApplicationBuilder app)
        {
            try
            {
                app.UseStatusCodePages(async ctx =>
                {
                    if (ctx.HttpContext.Response.StatusCode == 404 && ctx.HttpContext.Request.Path.Value.Contains("logo"))
                        ctx.HttpContext.Response.Redirect("/public.png");
                });
                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot2")),
                    RequestPath = new PathString("/logo"),
                    OnPrepareResponse = ctx =>
                    {
                        ctx.Context.Response.Headers.Add("Cache-Control", "no-store");
                        var file = ctx.File.Name.Split(".")[0];
                        var identity = ctx.Context.User.Identities.FirstOrDefault();
                        string UserId = identity?.FindFirst(x => x.Type == ClaimTypes.Name)?.Value ?? "";
                        if (UserId != file)
                        {
                            // respond HTTP 401 Unauthorized with empty body.
                            ctx.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            ctx.Context.Response.ContentLength = 0;
                            ctx.Context.Response.Body = Stream.Null;

                            // - or, redirect to another page. -
                            // ctx.Context.Response.Redirect("/");
                        }
                    }
                });

            }
            catch (Exception ex)
            {

            }
        }
    }
}
