﻿namespace LogRequestMiddleware.Services
{
    public interface IUserService
    {
        List<TestUser> Users { get; set; }
    }
    public class UserService : IUserService
    {
        public UserService()
        {
            Users = new List<TestUser>()
            {
                new TestUser() {Id=1,Name="Jimmy",Email="test1@test.com",Password="qwer"},
                new TestUser() {Id=2,Name="Tommy",Email="test2@test.com",Password="asdf"},
                new TestUser() {Id=3,Name="Jane",Email="test3@test.com",Password="zxcv"},
            };
        }
        public List<TestUser> Users { get; set; }
    }
    public class TestUser
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
    }
}
