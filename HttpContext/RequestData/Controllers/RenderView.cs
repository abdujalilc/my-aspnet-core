﻿using http_accessor.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace http_accessor.Controllers
{
    public class RenderView : Controller
    {
        public async Task<IActionResult> TestViewRender()
        {
            string context_html = await ManualViewEngine.RenderViewToStringAsync(
                "Index",
                "world",
                this.ControllerContext,
                false
                );
            return Content(context_html,"text/html");
        }
    }
}
