﻿using CustomMiddleware.Services;

namespace CustomMiddleware
{
    public class MyMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IGreeter _greeter;

        public MyMiddleware(RequestDelegate next, IGreeter greeter)
        {
            _next = next;
            _greeter = greeter;
        }

        public async Task Invoke(HttpContext context)
        {
            await context.Response.WriteAsync(_greeter.Greet()+ " _MyMiddleware");
            await _next(context);
        }
    }
}
