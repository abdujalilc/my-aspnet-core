using CustomMiddleware;
using CustomMiddleware.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddTransient<IGreeter, Greeter>();

var app = builder.Build();

// IApplicationBuilder.Use
app.Use(
    next =>
    {
        return async ctx =>
        {
            await ctx.Response.WriteAsync("Hello from IApplicationBuilder.Use!\n");
            await next(ctx);
        };
    });

// Use extension
app.Use(async (ctx, next) =>
{
    await ctx.Response.WriteAsync("Hello from extension method Use!\n");
    await next();
});

// Use extension with RequestServices
app.Use(
    async (ctx, next) =>
    {
        var greeter = ctx.RequestServices.GetService<IGreeter>();
        await ctx.Response.WriteAsync(greeter.Greet()+ " ctx.RequestServices.GetService\n");
        await next();
    });

app.UseMiddleware<MyMiddleware>();

// Run extension
app.Run(async context => await context.Response.WriteAsync("Hello from extension method Run!\n"));
app.Run();
