﻿namespace CustomMiddleware.Services
{
    public class Greeter : IGreeter
    {
        public string Greet()
        {
            return "Hello from Greeter!";
        }
    }

    public interface IGreeter
    {
        string Greet();
    }
}
