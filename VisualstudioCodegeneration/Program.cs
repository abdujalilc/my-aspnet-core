using Microsoft.EntityFrameworkCore;
using visualstudio_codegeneration.Data;

var builder = WebApplication.CreateBuilder(args);

/*builder.Services.AddDbContext<MvcMovieContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("MvcMovieContext")));*/

string? con_string = "Data Source=AppData\\QuestionnaireDB.db";
builder.Services.AddSqlite<MvcMovieContext>(con_string);


builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages().AddRazorRuntimeCompilation();

var app = builder.Build();

app.UseStaticFiles();
app.UseRouting();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Movies}/{action=Index}/{id?}");
app.MapRazorPages();

app.Run();