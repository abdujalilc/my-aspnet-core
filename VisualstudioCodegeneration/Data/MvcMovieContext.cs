﻿using Microsoft.EntityFrameworkCore;
using visualstudio_codegeneration.Models;

namespace visualstudio_codegeneration.Data
{
    public class MvcMovieContext : DbContext
    {
        public MvcMovieContext(DbContextOptions<MvcMovieContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movie { get; set; }
    }
}
