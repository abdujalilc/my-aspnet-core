﻿using Microsoft.AspNetCore.Mvc;

namespace popup_notifications.Controllers
{
    public class ModalPopupController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public  IActionResult Success()
        {
            TempData["Success"] = "Something is successful";
            return RedirectToAction("Index");
        }
        public IActionResult Info()
        {
            TempData["Info"] = "Info about something";
            return RedirectToAction("Index");
        }
        public IActionResult Error()
        {
            TempData["Error"] = "Some error happened";
            return RedirectToAction("Index");
        }
    }
}
