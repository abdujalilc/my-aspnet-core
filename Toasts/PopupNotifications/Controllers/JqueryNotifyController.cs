﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace popup_notifications.Controllers
{
    public class JqueryNotifyController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string test)
        {
            Console.WriteLine("Hello from console");
            Debug.WriteLine("hello from debug");
            TempData["Message"] = " Customer updated successfully.";
            return View();
        }
    }
}
