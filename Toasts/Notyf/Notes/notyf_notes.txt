﻿Install-Package AspNetCoreHero.ToastNotification

--------------------------------------------

<script src="~/js/site.js" asp-append-version="true"></script>
@await Component.InvokeAsync("Notyf") 
@RenderSection("Scripts", required: false)

--------------------------------------------

services.AddNotyf(config=> { config.DurationInSeconds = 10;config.IsDismissable = true;config.Position = NotyfPosition.BottomRight; });

--------------------------------------------

private readonly INotyfService _notyf;
public HomeController(INotyfService notyf)
{
    _notyf = notyf;
}

_notyf.Success("Success Notification");
_notyf.Success("Success Notification that closes in 10 Seconds.",3);

_notyf.Error("Some Error Message");
_notyf.Warning("Some Error Message");
_notyf.Information("Information Notification - closes in 4 seconds.", 4);

_notifyService.Custom("Custom Notification - closes in 5 seconds.", 5, "whitesmoke", "fa fa-gear");
_notifyService.Custom("Custom Notification - closes in 5 seconds.", 10, "#B600FF", "fa fa-home");
--------------------------------------------

app.UseNotyf();