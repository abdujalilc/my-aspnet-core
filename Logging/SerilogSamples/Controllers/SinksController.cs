﻿using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace SerilogSamples.Controllers
{
    public class SinksController : Controller
    {
        public IActionResult Index()
        {
            Log.Information("Ah, there you are!");
            return View();
        }
    }
}
