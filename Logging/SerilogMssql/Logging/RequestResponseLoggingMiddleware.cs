﻿using Serilog;
using Serilog.Parsing;
using System.Security.Claims;

namespace SerilogMssql.Logging
{
    public class RequestResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        public RequestResponseLoggingMiddleware(
            RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext context)
        {            
            var requestResponseLog = new RequestResponseLog
            {
                RequestTime = DateTimeOffset.UtcNow,
                Request = await FormatRequest(context,true)
            };

            await _next(context);

            requestResponseLog.ResponseTime = DateTimeOffset.UtcNow;
            requestResponseLog.Response = await FormatResponse(context,true);
            Log.Information(requestResponseLog.Request);
            //_logger.LogInformation(requestResponseLog.Request);
        }
        async Task<string> FormatRequest(HttpContext context, bool isDevelopmentEnvironment)
        {
            HttpRequest request = context.Request;
            var emailClaim = context.User.FindFirst(c => c.Type == ClaimTypes.Email);
            string userEmail = emailClaim != null ? emailClaim.Value : "No user";
            var logMessage = $"IP: {request.HttpContext.Connection.RemoteIpAddress.MapToIPv4()}, " +
                $"Schema: {request.Scheme}, Method: {request.Method}, " +
                $"User: {userEmail}, " +
                $"Host: {request.Host}, " +
                $"Path: {request.Path}";
            return logMessage;
        }
        async Task<string> FormatResponse(HttpContext context, bool isDevelopmentEnvironment)
        {
            HttpRequest request = context.Request;
            var emailClaim = context.User.FindFirst(c => c.Type == ClaimTypes.Email);
            string userEmail = emailClaim != null ? emailClaim.Value : "No user";
            var logMessage = $"IP: {request.HttpContext.Connection.RemoteIpAddress.MapToIPv4()}, " +
                $"Schema: {request.Scheme}, Method: {request.Method}, " +
                $"User: {userEmail}, " +
                $"Host: {request.Host}, " +
                $"Path: {request.Path}";
            return logMessage;
        }
    }
}
