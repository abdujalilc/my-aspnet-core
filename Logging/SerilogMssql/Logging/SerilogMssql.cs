﻿using Serilog.Sinks.MSSqlServer;
using Serilog;

namespace SerilogMssql.Logging
{
    public static class MySerilog
    {
        public static void AddSerilogServiceBasic()
        {
            Log.Logger = new LoggerConfiguration().WriteTo
            .MSSqlServer(
                connectionString: "Data Source=ACHULIEV;Initial Catalog=Serilog;User ID=sa;Password=P@ssword;Trust Server Certificate=True;",
                sinkOptions: new MSSqlServerSinkOptions { TableName = "LogEvents" })
            .CreateLogger();
        }
        public static void AddSerilogService()
        {
            var logDB = @"Data Source=ACHULIEV;Initial Catalog=Serilog;User ID=sa;Password=P@ssword;Trust Server Certificate=True;";
            var sinkOpts = new MSSqlServerSinkOptions();
            sinkOpts.TableName = "Logs";
            var columnOpts = new ColumnOptions();
            columnOpts.Store.Remove(StandardColumn.Properties);
            columnOpts.Store.Add(StandardColumn.LogEvent);
            columnOpts.LogEvent.DataLength = 2048;
            columnOpts.PrimaryKey = columnOpts.TimeStamp;
            columnOpts.TimeStamp.NonClusteredIndex = true;

            var log = new LoggerConfiguration()
                .WriteTo.MSSqlServer(
                    connectionString: logDB,
                    sinkOptions: sinkOpts,
                    columnOptions: columnOpts
                ).CreateLogger();
        }
        public static void AddSerilogWithConfig()
        {
            var appSettings = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var logDB = @"Data Source=ACHULIEV;Initial Catalog=Serilog;User ID=sa;Password=P@ssword;Trust Server Certificate=True;";
            var sinkOpts = new MSSqlServerSinkOptions { TableName = "Logs" };
            var columnOpts = new ColumnOptions();

            var log = new LoggerConfiguration()
                .WriteTo.MSSqlServer(
                    connectionString: logDB,
                    sinkOptions: sinkOpts,
                    columnOptions: columnOpts,
                    appConfiguration: appSettings
                ).CreateLogger();
        }
    }
}
