﻿using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace SerilogMssql.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            try
            {
                //_logger.LogInformation("Home page accessed.");
                //Log.Information("Ah, there you are!");
            }
            catch (Exception ex)
            {

            }

            return View();
        }
    }
}
