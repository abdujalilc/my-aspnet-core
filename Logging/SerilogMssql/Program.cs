using Serilog;
using Serilog.Sinks.MSSqlServer;
using SerilogMssql.Logging;
using System.Diagnostics;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

//builder.Host.UseSerilog();
Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Information()
            .WriteTo.MSSqlServer(
                "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=MySerilog;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False",
                sinkOptions: new MSSqlServerSinkOptions
                {
                    TableName = "LogEvents",
                    AutoCreateSqlTable = true,
                    AutoCreateSqlDatabase = true,                    
                })
            .CreateLogger();
Serilog.Debugging.SelfLog.Enable(msg =>
{
    Debug.Print(msg);
    Debugger.Break();
});
//MySerilog.AddSerilogServiceBasic();
//MySerilog.AddSerilogWithConfig();

var app = builder.Build();

app.UseStaticFiles();
app.UseMiddleware<RequestResponseLoggingMiddleware>();
app.UseRouting();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

app.Run();