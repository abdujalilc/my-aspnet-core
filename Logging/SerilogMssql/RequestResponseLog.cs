﻿namespace SerilogMssql
{
    public class RequestResponseLog
    {
        public string Request { get; set; }
        public DateTimeOffset RequestTime { get; set; }
        public string Response { get; set; }
        public DateTimeOffset ResponseTime { get; set; }
    }
}
